Feature: Pages
	Background:
		Given cities:
			| name   |  
			| Moscow |  
			| Paris  |  
			| Berlin |  
			| Tokyo  |  
			| Genoa  |  
			| Rimini |  
		And categories:
			| name    |  
			| cities  |  
			| museums |  
			| sea     |  
			| beaches |  
			| nature  |  
		And excursions:
			| name    | description | published | city   | categories           |  
			| moscow1 |             | true      | Moscow | cities, museums      |  
			| moscow2 |             | false     | Moscow | cities, museums      |  
			| moscow3 |             | true      | Moscow | museums              |  
			| moscow4 |             | true      | Moscow | museums              |  
			| moscow5 |             | true      | Moscow | cities, museums      |  
			| moscow6 |             | true      | Moscow | cities, museums      |  
			| moscow7 |             | true      | Moscow | cities, museums      |  
			| moscow8 |             | true      | Moscow | cities, museums      |  
			| paris1  |             | true      | Paris  | cities, museums      |  
			| paris2  |             | false     | Paris  | cities, museums      |  
			| rimini1 |             | true      | Rimini | sea, beaches, nature |  
		When I visit "/"

	Scenario: Cities page
		Then I see cities:
		  | name   |  
		  | Moscow |  
		  | Paris  |  
		  | Rimini |  
		And I don't see cities:
		  | name   |  
		  | Berlin |  
		  | Tokyo  |  
		  | Genoa  |  
		And I see excursions:
			| name    |  
			| moscow1 |  
			| moscow3 |  
			| moscow4 |  
			| moscow5 |  
			| moscow6 |  
			| paris1  |  
			| rimini1 |  
		And I don't see excursions:
			| name    |  
			| moscow2 |  
			| moscow7 |  
			| moscow8 |  
			| paris2  |  

	Scenario: City page
		When I click "Moscow"
		Then I am redirected to "/cities/1"
		And I see excursions:
			| name    |  
			| moscow1 |  
			| moscow3 |  
			| moscow4 |  
			| moscow5 |  
			| moscow6 |  
			| moscow7 |  
			| moscow8 |  
		And I don't see excursions:
			| name    |  
			| moscow2 |  
			| paris1  |  
			| paris2  |  
			| rimini1 |  
		And I see categories:
			| name    |  
			| cities  |  
			| museums |  
		And I don't see categories:
			| name    |  
			| sea     |  
			| beaches |  
			| nature  |  

	Scenario: Category page
		When I click "Moscow"
		And I click first "cities"
		Then I am redirected to "/categories/1"
		And I see excursions:
			| name    |  
			| moscow1 |  
			| moscow5 |  
			| moscow6 |  
			| moscow7 |  
			| moscow8 |  
			| paris1  |  
		And I don't see excursions:
			| name    |  
			| moscow2 |  
			| moscow3 |  
			| moscow4 |  
			| paris2  |  
			| rimini1 |  
		Then I see cities:
		  | name   |  
		  | Moscow |  
		  | Paris  |  
		And I don't see cities:
		  | name   |  
		  | Berlin |  
		  | Tokyo  |  
		  | Genoa  |  
		  | Rimini |  

  Scenario: Excursion page
  	When I click "moscow3"
  	Then I am redirected to "/excursions/3"
  	And I see text "moscow3"
  	And I see text "Moscow"
  	And I see text "museums"
