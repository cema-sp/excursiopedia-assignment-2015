Given(/^cities:$/) do |cities_table|
  cities_table.hashes.each do |city_hash|
    create(:city, city_hash)
  end
end

Given(/^categories:$/) do |categories_table|
  categories_table.hashes.each do |category_hash|
    create(:category, category_hash)
  end
end

Given(/^excursions:$/) do |excursions_table|
  excursions_table.hashes.each do |excursion_hash|
    excursion_hash[:city] = City.find_by(name: excursion_hash[:city])
    excursion_hash[:categories] =
      excursion_hash[:categories]
      .split(', ')
      .map { |name| Category.find_by(name: name) }

    create(:excursion, excursion_hash)
  end
end
