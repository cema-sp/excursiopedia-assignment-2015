Then(/^I see "([^"]*)" menu item$/) do |item_name|
  expect(page).to have_css 'nav li', text: item_name
end

Then(/^I don't see "([^"]*)" menu item$/) do |item_name|
  expect(page).not_to have_css 'nav li', text: item_name
end

Then(/^I see "([^"]*)" form$/) do |form_action|
  expect(page).to have_xpath("//form[@action=\"/#{form_action}\"]")
end

Then(/^I see "([^"]*)" field$/) do |field_name|
  expect(page).to have_field field_name
end

Then(/^I see "([^"]*)" button$/) do |button_name|
  expect(page).to have_selector(:link_or_button, button_name)
end

Then(/^I see "([^"]*)" link$/) do |link_name|
  expect(page).to have_link link_name
end

Then(/^I see "([^"]*)" select with options:$/) do |select_name, options|
  expect(page)
    .to have_select(select_name, with_options: options.raw.flatten)
end

Then(/^I see text "([^"]*)"$/) do |text|
  expect(page).to have_content text
end

Then(/^I see cities:$/) do |cities_table|
  cities_table.hashes.each do |city_hash|
    expect(page).to have_css '.city h1', text: /#{city_hash[:name]}/
  end
end

Then(/^I don't see cities:$/) do |cities_table|
  cities_table.hashes.each do |city_hash|
    expect(page).not_to have_css '.city h1', text: /#{city_hash[:name]}/
  end
end

Then(/^I see excursions:$/) do |excursions_table|
  excursions_table.hashes.each do |excursion_hash|
    expect(page).to have_css '.excursion h1', text: /#{excursion_hash[:name]}/
  end
end

Then(/^I don't see excursions:$/) do |excursions_table|
  excursions_table.hashes.each do |excursion_hash|
    expect(page).not_to have_css '.excursion h1', text: /#{excursion_hash[:name]}/
  end
end

Then(/^I see categories:$/) do |categories_table|
  categories_table.hashes.each do |category_hash|
    expect(page).to have_css '.category h1', text: /#{category_hash[:name]}/
  end
end

Then(/^I don't see categories:$/) do |categories_table|
  categories_table.hashes.each do |category_hash|
    expect(page).not_to have_css '.category h1', text: /#{category_hash[:name]}/
  end
end
