Then(/^I am redirected to "([^"]*)"$/) do |path|
  expect(current_path).to eq(path)
end

When(/^I visit "([^"]*)"$/) do |path|
  visit path
end

When(/^I click "([^"]*)"$/) do |link_name|
  click_link_or_button link_name, match: :smart
end

When(/^I click first "([^"]*)"$/) do |link_name|
  click_link_or_button link_name, match: :first
end

When(/^I click "([^"]*)" munu item$/) do |item_name|
  within(:css, 'nav') do
    click_link_or_button item_name, match: :smart
  end
end

When(/^I fill "([^"]*)" "([^"]*)" field with "([^"]*)"$/) do |field, action, value|
  within(:xpath, '//form[@action="/' + action + '"]') do
    fill_in field, with: value, match: :first
  end
end

When(/^I click "([^"]*)" form "([^"]*)"$/) do |action, link_or_button|
  within(:xpath, '//form[@action="/' + action + '"]') do
    click_button link_or_button, match: :smart
  end
end

When(/^I select "([^"]*)" from "([^"]*)" "([^"]*)" select$/) do |option_name, select_name, action|
  within(:xpath, '//form[@action="/' + action + '"]') do
    select option_name, from: select_name
  end
end
