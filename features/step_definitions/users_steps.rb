Given(/^registered users:$/) do |users_table|
  users_table.hashes.each do |user_hash|
    create(:user, user_hash)
  end
end
