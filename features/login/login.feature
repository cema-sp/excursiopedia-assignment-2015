Feature: Login
	Background:
		Given registered users:
			| name    | email               | type    | password |  
			| admin   | admin@example.com   | Admin   | password |  
			| manager | manager@example.com | Manager | password |  
		When I visit "/"

	Scenario: Open login page
		Then I see "Log in" menu item
		When I click "Log in" munu item
		Then I see "users/login" form
		And I see "Email" field
		And I see "Password" field
		And I see "Log in" button
		And I see "Sign up" link

	Scenario: Log in as admin
		When I click "Log in" munu item
		And I fill "Email" "users/login" field with "admin@example.com"
		And I fill "Password" "users/login" field with "password"
		When I click "users/login" form "Log in"
		Then I am redirected to "/admin/excursions"
		And I see "Manage" menu item
		And I see "admin (Admin)" menu item
		And I see "Profile" menu item
		And I see "Log out" menu item
		And I don't see "Log in" menu item
		And I don't see "Sign up" menu item

	Scenario: Log in as manager
		When I click "Log in" munu item
		And I fill "Email" "users/login" field with "manager@example.com"
		And I fill "Password" "users/login" field with "password"
		When I click "users/login" form "Log in"
		Then I am redirected to "/admin/excursions"
		And I see "Manage" menu item
		And I see "Complete!" menu item
		And I see "manager (Manager)" menu item
		And I see "Profile" menu item
		And I see "Log out" menu item
		And I don't see "Log in" menu item
		And I don't see "Sign up" menu item

	Scenario: Open signup page
		Then I see "Sign up" menu item
		When I click "Sign up" munu item
		Then I see "users" form
		And I see "Name" field
		And I see "Email" field
		And I see "Type" select with options:
			| Admin   |  
			| Manager |  
		And I see "Password" field
		And I see "Password confirmation" field
		And I see "Sign up" button
		And I see "Log in" link

	Scenario: Sign up as admin
		When I click "Sign up" munu item
		When I fill "Name" "users" field with "New Admin"
		And I fill "Email" "users" field with "newadmin@example.com"
		And I select "Admin" from "Type" "users" select
		And I fill "Password" "users" field with "password"
		And I fill "Password confirmation" "users" field with "password"
		When I click "users" form "Sign up"
		Then I am redirected to "/admin/excursions"
		And I see "New Admin (Admin)" menu item
		And I see "Log out" menu item
		And I don't see "Log in" menu item
		And I don't see "Sign up" menu item
