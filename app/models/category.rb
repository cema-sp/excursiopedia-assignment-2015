class Category < ActiveRecord::Base
  validates :name,
            presence: true,
            length: { minimum: 2, maximum: 32 }

  has_and_belongs_to_many :excursions, -> { uniq }
end
