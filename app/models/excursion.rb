class Excursion < ActiveRecord::Base
  has_paper_trail on: :update, only: :description

  validates :name,
            presence: true,
            length: { minimum: 2, maximum: 120 }

  validates :city_id,
            presence: true

  validates :category_ids,
            presence: true,
            length: { minimum: 1 }

  belongs_to :city
  has_and_belongs_to_many :categories, -> { uniq }

  scope :published, -> { where(published: true) }
end
