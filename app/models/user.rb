class User < ActiveRecord::Base
  devise :database_authenticatable, :registerable,
         :rememberable, :validatable

  validates :name,
            presence: true,
            length: { minimum: 4, maximum: 32 }

  def is_admin?
    type == 'Admin'
  end

  def is_manager?
    type == 'Manager'
  end

  def self.types
    %w(Admin Manager)
  end
end
