class City < ActiveRecord::Base
  validates :name,
            presence: true,
            length: { minimum: 2, maximum: 32 }

  has_many :excursions, dependent: :destroy

  scope :has_excursions,
        -> { includes(:excursions).select { |c| c.excursions.published.count > 0 } }
end
