class CityPolicy < ApplicationPolicy
  def index?
    user && (user.is_manager? || user.is_admin?)
  end

  def create?
    user && user.is_admin?
  end

  alias_method :show?, :index?
  alias_method :update?, :create?
  alias_method :destroy?, :create?

  class Scope < Scope
    def resolve
      scope
    end
  end
end
