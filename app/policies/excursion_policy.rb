class ExcursionPolicy < ApplicationPolicy
  def create?
    user && user.is_admin?
  end

  def update?
    user && (user.is_admin? || user.is_manager?)
  end

  alias_method :destroy?, :create?
  alias_method :index?, :update?
  alias_method :show?, :update?
  alias_method :complete?, :update?

  class Scope < Scope
    def resolve
      scope
    end
  end
end
