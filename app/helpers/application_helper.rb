module ApplicationHelper
  def flash_class(level)
    case level
    when 'notice' then 'alert alert-dismissable alert-info'
    when 'success' then 'alert alert-dismissable alert-success'
    when 'error' then 'alert alert-dismissable alert-danger'
    when 'alert' then 'alert alert-dismissable alert-danger'
    end
  end

  def categories_links(excursion, scope = nil)
    excursion.categories.map do |category|
      link_to([scope, category], class: ['category']) do
        content_tag(:h1, category.name)
      end
    end.join(', ').html_safe
  end

  def diff(content1, content2)
    changes = Diffy::Diff.new(
      content1,
      content2,
      include_plus_and_minus_in_html: true,
      include_diff_info: true
    )

    changes.to_s.present? ? changes.to_s(:html).html_safe : 'No Changes'
  end
end
