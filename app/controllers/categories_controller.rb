class CategoriesController < ApplicationController
  # GET /categories/1
  def show
    @category = Category.find(params[:id])
  end
end
