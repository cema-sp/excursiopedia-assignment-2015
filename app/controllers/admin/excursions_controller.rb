class Admin::ExcursionsController < ApplicationController
  before_action :authenticate_user!
  before_action :authenticate_admin!,
                except: [:index, :show, :edit, :update, :complete]

  before_action :set_paper_trail_whodunnit, only: :update

  after_action :verify_authorized

  before_action :set_excursion, only: [:show, :edit, :update, :destroy]

  # GET /excursions
  def index
    @excursions = Excursion.all
    authorize Excursion
  end

  # GET /excursions/1
  def show
    authorize @excursion
  end

  # GET /excursions/new
  def new
    @excursion = Excursion.new
    authorize @excursion
  end

  # GET /excursions/1/edit
  def edit
    authorize @excursion
  end

  # GET /excursions/complete
  def complete
    authorize Excursion
    @completing = true
    @excursion = (
      Excursion.where(description: nil).first ||
      Excursion.where(description: '').first ||
      Excursion.where(description: ' ').first
    )

    if @excursion
      render :edit
    else
      redirect_to admin_excursions_path
    end
  end

  # POST /excursions
  def create
    @excursion = Excursion.new(excursion_params)
    authorize @excursion

    respond_to do |format|
      if @excursion.save
        format.html { redirect_to [:admin, @excursion], notice: 'Excursion was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /excursions/1
  def update
    authorize @excursion
    respond_to do |format|
      if @excursion.update(excursion_params)
        format.html do
          if params[:completing]
            redirect_to(action: :complete)
            return
          else
            redirect_to [:admin, @excursion],
                        notice: 'Excursion was successfully updated.'
          end
        end
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /excursions/1
  def destroy
    authorize @excursion
    @excursion.destroy
    respond_to do |format|
      format.html { redirect_to admin_excursions_url, notice: 'Excursion was successfully destroyed.' }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_excursion
    @excursion = Excursion.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def excursion_params
    params
      .require(:excursion)
      .permit(:name, :description, :published, :city_id, category_ids: [])
  end
end
