class Admin::VersionsController < ApplicationController
  before_action :set_excursion_and_version, only: :diff

  def diff
  end

  private

  def set_excursion_and_version
    @excursion = Excursion.find(params[:excursion_id])
    @version = @excursion.versions.find(params[:id])
  end
end
