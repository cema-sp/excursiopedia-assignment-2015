class Admin::CitiesController < ApplicationController
  before_action :authenticate_user!
  before_action :authenticate_admin!, except: [:index, :show]

  after_action :verify_authorized

  before_action :set_city, only: [:show, :edit, :update, :destroy]

  # GET /cities
  def index
    @cities = City.all
    authorize City
  end

  # GET /cities/1
  def show
    authorize @city
  end

  # GET /cities/new
  def new
    @city = City.new
    authorize @city
  end

  # GET /cities/1/edit
  def edit
    authorize @city
  end

  # POST /cities
  def create
    @city = City.new(city_params)
    authorize @city

    respond_to do |format|
      if @city.save
        format.html { redirect_to admin_cities_path, notice: 'City was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /cities/1
  def update
    authorize @city
    respond_to do |format|
      if @city.update(city_params)
        format.html { redirect_to admin_cities_path, notice: 'City was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /cities/1
  def destroy
    authorize @city
    @city.destroy
    respond_to do |format|
      format.html { redirect_to admin_cities_url, notice: 'City was successfully destroyed.' }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_city
    @city = City.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def city_params
    params.require(:city).permit(:name)
  end
end
