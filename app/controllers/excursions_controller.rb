class ExcursionsController < ApplicationController
  # GET /excursions/1
  def show
    @excursion = Excursion.find(params[:id])

    session[:visited_excursions] ||= []
    session[:visited_excursions].reject! { |id| id == params[:id].to_i }

    @visited_excursions =
      session[:visited_excursions]
      .dup
      .reverse[0, 3]
      .map { |id| Excursion.find(id) }

    session[:visited_excursions] << @excursion.id
  end

  # DELETE /excursions/1
  def erase
    session[:visited_excursions].reject! { |id| id == params[:id].to_i }
    redirect_to :back
  end
end
