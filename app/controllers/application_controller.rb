class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  include Pundit

  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  # ---------- Define current_admin & current_manager -------------------
  User.types.each do |k|
    define_method "current_#{k.underscore}" do
      current_user if current_user.is_a?(k.constantize)
    end

    define_method "authenticate_#{k.underscore}!" do |_opts = {}|
      send("current_#{k.underscore}") || user_not_authorized
    end
  end
  # ------------------ end define ---------------------------------------

  # Redirect rule for sign in
  def after_sign_in_path_for(resource)
    stored_location_for(resource) ||
      if resource.is_a?(User)
        admin_excursions_path
      else
        super
      end
  end

  # Redirect rule for sign up
  def after_sign_up_path_for(resource)
    if resource.is_a?(User)
      admin_excursions_path
    else
      super
    end
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) << :name
    devise_parameter_sanitizer.for(:sign_up) << :type
    devise_parameter_sanitizer.for(:account_update) << :name
    # devise_parameter_sanitizer.for(:account_update) << :type
  end

  def user_not_authorized
    flash[:alert] = 'You are not authorized to do this action.'
    if user_signed_in?
      redirect_to user_root_url
    else
      redirect_to root_url
      # head :forbidden
    end
  end
end
