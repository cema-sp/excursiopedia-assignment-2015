class CitiesController < ApplicationController
  # GET /cities
  def index
    @cities = City.has_excursions
  end

  # GET /cities/1
  def show
    @city = City.find(params[:id])
  end
end
