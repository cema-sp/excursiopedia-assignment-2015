class CreateExcursions < ActiveRecord::Migration
  def change
    create_table :excursions do |t|
      t.string :name
      t.text :description
      t.boolean :published
      t.references :city, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
