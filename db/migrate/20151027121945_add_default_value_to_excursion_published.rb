class AddDefaultValueToExcursionPublished < ActiveRecord::Migration
  def up
    change_column :excursions, :published, :boolean, default: false
  end

  def down
    fail ActiveRecord::IrreversibleMigration, "Can't remove the default"
  end
end
