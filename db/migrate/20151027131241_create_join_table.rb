class CreateJoinTable < ActiveRecord::Migration
  def change
    create_join_table :excursions, :categories do |t|
      t.index :excursion_id
      t.index :category_id
    end
  end
end
