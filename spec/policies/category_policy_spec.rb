require 'rails_helper'

describe CategoryPolicy do
  subject { described_class }

  permissions :index?, :show? do
    context 'for user' do
      let!(:user) { create(:user) }

      it 'denies access' do
        expect(subject).not_to permit(user, City)
      end
    end

    context 'for admin' do
      let!(:user) { create(:user, :admin) }

      it 'grants access' do
        expect(subject).to permit(user, City)
      end
    end

    context 'for manager' do
      let!(:user) { create(:user, :manager) }

      it 'grants access' do
        expect(subject).to permit(user, City)
      end
    end
  end

  permissions :create?, :update?, :destroy? do
    context 'for user' do
      let!(:user) { create(:user) }

      it 'denies access' do
        expect(subject).not_to permit(user, City)
      end
    end

    context 'for admin' do
      let!(:user) { create(:user, :admin) }

      it 'grants access' do
        expect(subject).to permit(user, City)
      end
    end

    context 'for manager' do
      let!(:user) { create(:user, :manager) }

      it 'denies access' do
        expect(subject).not_to permit(user, City)
      end
    end
  end
end
