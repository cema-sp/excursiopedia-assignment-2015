require 'rails_helper'

RSpec.describe Excursion, type: :model do
  describe 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_length_of(:name).is_at_least(2).is_at_most(120) }
    it { should validate_presence_of(:city_id) }
    it { should validate_presence_of(:category_ids) }
    # it { should validate_length_of(:categories).is_at_least(1) }
  end

  describe 'relations' do
    it { should have_and_belong_to_many(:categories) }
  end
end
