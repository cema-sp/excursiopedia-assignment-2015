require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_length_of(:name).is_at_least(4).is_at_most(32) }
    it { should validate_presence_of(:email) }
  end

  describe 'instance methods' do
    describe '#is_admin?' do
      subject { user.is_admin? }

      context 'for admin' do
        let(:user) { create(:user, :admin) }
        it { should be true }
      end

      context 'for manager' do
        let(:user) { create(:user, :manager) }
        it { should be false }
      end

      context 'for user' do
        let(:user) { create(:user) }
        it { should be false }
      end
    end

    describe '#is_manager?' do
      subject { user.is_manager? }

      context 'for admin' do
        let(:user) { create(:user, :admin) }
        it { should be false }
      end

      context 'for manager' do
        let(:user) { create(:user, :manager) }
        it { should be true }
      end

      context 'for user' do
        let(:user) { create(:user) }
        it { should be false }
      end
    end
  end
end
