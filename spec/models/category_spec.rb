require 'rails_helper'

RSpec.describe Category, type: :model do
  describe 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_length_of(:name).is_at_least(2).is_at_most(32) }
    # it { should validate_uniqueness_of(:excursions) }
  end

  describe 'relations' do
    it { should have_and_belong_to_many(:excursions) }
  end
end
