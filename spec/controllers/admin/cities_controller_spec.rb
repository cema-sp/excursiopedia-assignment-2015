require 'rails_helper'

RSpec.describe Admin::CitiesController, type: :controller do
  let!(:admin) { create(:user, :admin) }

  before { sign_in :user, admin }

  describe 'GET #index' do
    let (:cities) { create_list(:city, 3) }

    subject { get :index }
    before { subject }

    it 'assigns all cities as @cities' do
      expect(assigns(:cities)).to eq(cities)
    end
  end

  describe 'GET #show' do
    let (:city) { create(:city) }

    subject { get :show, id: city.id }
    before { subject }

    it 'assigns the requested city as @city' do
      expect(assigns(:city)).to eq(city)
    end
  end

  describe 'GET #new' do
    subject { get :new }
    before { subject }

    it 'assigns a new city as @city' do
      expect(assigns(:city)).to be_a_new(City)
    end
  end

  describe 'GET #edit' do
    let (:city) { create(:city) }

    subject { get :edit, id: city.id }
    before { subject }

    it 'assigns the requested city as @city' do
      expect(assigns(:city)).to eq(city)
    end
  end

  describe 'POST #create' do
    subject { post :create, city: attributes }

    context 'with valid params' do
      let (:attributes) { attributes_for(:city) }

      describe 'on action' do
        it 'creates a new City' do
          expect { subject }.to change(City, :count).by(1)
        end
      end

      describe 'after action' do
        before { subject }

        it 'assigns a newly created city as @city' do
          expect(assigns(:city)).to be_a(City)
          expect(assigns(:city)).to be_persisted
        end

        it 'redirects to cities list' do
          expect(response).to redirect_to(admin_cities_url)
        end
      end
    end

    context 'with invalid params' do
      let (:attributes) { attributes_for(:city, name: '') }

      describe 'on action' do
        it 'does not create a new City' do
          expect { subject }.not_to change(City, :count)
        end
      end

      describe 'after action' do
        before { subject }

        it 'assigns a newly created but unsaved city as @city' do
          expect(assigns(:city)).to be_a City
        end

        it "re-renders the 'new' template" do
          expect(response).to render_template('new')
        end
      end
    end
  end

  describe 'PUT #update' do
    let (:city) { create(:city) }

    subject { put :update, id: city.id, city: attributes }

    before { subject }

    context 'with valid params' do
      let (:attributes) { attributes_for(:city) }

      it 'updates the requested city' do
        expect(city.reload.name).to eq(attributes[:name])
      end

      it 'redirects to cities list' do
        expect(response).to redirect_to(admin_cities_url)
      end
    end

    context 'with invalid params' do
      let (:attributes) { attributes_for(:city, name: '') }

      it 'assigns the city as @city' do
        expect(assigns(:city)).to eq(city)
      end

      it "re-renders the 'edit' template" do
        expect(response).to render_template('edit')
      end
    end
  end

  describe 'DELETE #destroy' do
    let!(:city) { create(:city) }

    subject { delete :destroy, id: city.id }

    describe 'on action' do
      it 'destroys the requested city' do
        expect { subject }.to change(City, :count).by(-1)
      end
    end

    describe 'after action' do
      before { subject }

      it 'redirects to the cities list' do
        expect(response).to redirect_to(admin_cities_url)
      end
    end
  end
end
