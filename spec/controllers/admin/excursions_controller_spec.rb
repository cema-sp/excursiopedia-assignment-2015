require 'rails_helper'

RSpec.describe Admin::ExcursionsController, type: :controller do
  let!(:manager) { create(:user, :admin) }

  before { sign_in :user, manager }

  describe 'GET #complete' do
    subject { get :complete }

    context 'no empty descriptions' do
      let!(:excursions) { create_list(:excursion, 3) }

      it 'redirects to excursions' do
        expect(subject).to redirect_to admin_excursions_path
      end
    end

    context 'empty descriptions exist' do
      let!(:excursion) { create(:excursion, description: ' ') }
      let!(:excursions) { create_list(:excursion, 3) }

      it 'renders edit action' do
        expect(subject).to render_template :edit
      end

      describe 'after action' do
        before { subject }

        it 'assigns excursion as @excursion' do
          expect(assigns(:excursion)).to eq(excursion)
        end

        it 'assigns @completing to true' do
          expect(assigns(:completing)).to eq(true)
        end
      end
    end
  end
end
