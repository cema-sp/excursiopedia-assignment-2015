require 'rails_helper'

RSpec.describe CitiesController, type: :controller do
  describe 'GET #index' do
    let!(:cities_w_excursions) { create_list(:city, 3) }
    let!(:cities_wo_excursions) { create_list(:city, 3) }

    subject { get :index }

    before do
      cities_w_excursions.each do |c|
        create(:excursion, city: c, published: true)
      end
      subject
    end

    it 'assigns all cities as @cities' do
      expect(assigns(:cities)).to match_array(cities_w_excursions)
    end
  end

  describe 'GET #show' do
    let (:city) { create(:city) }

    subject { get :show, id: city.id }
    before { subject }

    it 'assigns the requested city as @city' do
      expect(assigns(:city)).to eq(city)
    end
  end
end
