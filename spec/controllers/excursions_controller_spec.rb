require 'rails_helper'

RSpec.describe ExcursionsController, type: :controller do
  describe 'GET #show' do
    let!(:excursion) { create(:excursion) }
    let!(:excursions) { create_list(:excursion, 4) }

    subject { get :show, { id: excursion.id }, session }

    before { subject }

    context 'with empty session' do
      let!(:session) { {} }

      it 'assigns the requested excursion as @excursion' do
        expect(assigns(:excursion)).to eq(excursion)
      end

      it 'assigns [] to @visited_excursions' do
        expect(assigns(:visited_excursions)).to eq([])
      end
    end

    context 'with session' do
      let!(:session) do
        { visited_excursions: [
          excursions[0].id,
          excursions[2].id,
          excursion.id,
          excursions[3].id,
          excursions[1].id
        ] }
      end

      it 'assigns the requested excursion as @excursion' do
        expect(assigns(:excursion)).to eq(excursion)
      end

      it 'assigns visited to @visited_excursions' do
        expect(assigns(:visited_excursions))
          .to match_array([
            excursions[1],
            excursions[3],
            excursions[2]
          ])
      end
    end
  end

  describe 'DELETE #erase' do
    let!(:excursion) { create(:excursion) }
    let!(:excursions) { create_list(:excursion, 4) }

    subject { delete :erase, { id: excursion.id }, session }

    before do
      request.env['HTTP_REFERER'] = '/'
      subject
    end

    context 'with session' do
      let!(:session) do
        { visited_excursions: [
          excursions[0].id,
          excursions[2].id,
          excursion.id,
          excursions[3].id,
          excursions[1].id
        ] }
      end

      it 'assigns session w/o requested' do
        expect(session[:visited_excursions])
          .to match_array([
            excursions[0].id,
            excursions[2].id,
            excursions[3].id,
            excursions[1].id
          ])
      end
    end
  end
end
