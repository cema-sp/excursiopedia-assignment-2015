FactoryGirl.define do
  factory :excursion do
    name { Faker::Company.catch_phrase }
    description { Faker::Lorem.paragraph }
    published false
    city
    categories { create_list(:category, 2) }
  end
end
