FactoryGirl.define do
  factory :user do
    name { Faker::Name.name }
    email { Faker::Internet.email(name) }
    password 'password'

    trait :admin do
      type 'Admin'
    end

    trait :manager do
      type 'Manager'
    end
  end
end
