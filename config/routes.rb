Rails.application.routes.draw do
  namespace :admin do
    get 'versions/diff'
  end

  root 'cities#index'

  devise_for :users,
             path_names: {
               sign_in: 'login',
               sign_out: 'logout',
               sign_up: 'signup'
             }

  resources :cities, only: [:index, :show]
  resources :categories, only: :show

  resources :excursions, only: :show do
    member do
      delete :erase
    end
  end

  authenticated :user do
    root to: 'admin/excursions#index', as: :user_root

    namespace :admin do
      resources :cities
      resources :categories

      resources :excursions do
        collection do
          get :complete
        end

        resources :versions, only: [] do
          member do
            get :diff
          end
        end
      end
    end
  end
end
